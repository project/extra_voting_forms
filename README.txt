
Welcome to Extra Voting Forms!

This module gives you easy-to-use, and yet powerful, voting forms. It derives
from the now defunct simple_karma.module. [1]

Some features:

- Javascript voting; however, the module works 1000% even if the
user doesn't have Javascript

- Abuse control. The module allows you to limit users on how many points
they can give. The admin can also decide on how many different
comments/nodes a user can vote on

- Bonus voting powers to people with specific permissions. When a user has them
(because they are associated to a role, for example), then the user can give/take
more votes on each click.

- Everything is configurable from the admin interface

- Themable. The voting forms can look however you like. Two examples
are provided.

- Ability to put the forms in the link section of the comment/node
or anywhere in the comment template (by hand)

Enjoy!

[1]
Simple_karma was developed when Tony Mobily decided, mistakenly, to
develop a system to vote on nodes and comments from scratch without using
the powerful VotingAPIs.

Tony Mobily shamefully (no sarcasm, and he says so himself) duplicated efforts.
Even worse, he stuffed a lot of functionalities into simple_karma, where
the right thing to do would have been split the module into smaller ones - something
that is happening right now. Apologies, Drupal community...

When people started using simple_karma, and started getting very confused, he
decided to reform and conform.

He realised that there was a huge Deja-vu effect while looking at the sources
of simple_karma and the Voting API, and decided to kill simple_karma the
way it was before, and spread its features in different modules, basing the
voting mechanism on the powerful VotingAPIs.
